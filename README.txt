
-- SUMMARY --

The Scald: Facebook modules provides a way for site contributors to use the
badge they've generated on Facebook as Media Atoms, re-usable anywhere Media
Atoms can be dropped (Atom Reference fields, textfields that are Drag'n'Drop
enabled…)

For a full description of the module, visit its project page at
  http://drupal.org/sandbox/defr/1966848

See http://drupal.org/node/1895554 for a list of Scald providers as separate
projects for other great providers.

-- REQUIREMENTS --

* Scald module
  http://drupal.org/project/scald

-- INSTALLATION --

* To test it quickly with drush : drush en -y scald_flickr scald_dnd_library mee


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions :

  - Create atom of facebook_badge type

    Users in role with the "Create atom of facebook_badge type" will be able
    to add their badges as Media Atoms in Scald.

-- CONTACT --

Current maintainers :
* Franck Deroche (DeFr) - http://drupal.org/user/59710
* Pierre Ternon (pierre75) - http://drupal.org/user/236424
